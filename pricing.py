# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 11:52:11 2014

@author: dgevans
"""
import numpy as np

def riskFreePrice(Pi,lamb,gamma,beta):
    '''
    Computes the price of a risk free bond
    
    Inputs
    -------------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * beta - discount factor (float)

    Returns
    ---------
    * pf - Price of the risk free bond for each state of the world (length S array)
    * Rf - Return on the risk free bond for each state of the world
    '''
    pass
    
    E_Uchat(t+1)=Pi.dot(lamb_hat(t+1)**(-gamma))
    pf_hat(t)=beta*E_Uchat(t+1)
    m=np.ones(1)
    Rf(s)=m/pf(s)
    return pf,Rf
    
def stockPrice(Pi,lamb,gamma,beta):
    '''
    Computes the price to dividend ratio nu, and return on stock
    
    Inputs
    --------------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * beta - discount factor (float)
    
    Returns
    ----------
    * nu - Price to dividend ratio for each state of the world
    * Rs - Return on the stock from state s to sprime
    '''
    pass
    m=np.ones(1)
    vhat(t)=beta*E_[(vhat(t)+m)*lamb_hat(t+1)**(m-gamma)]
    
    S=len(lamb)
    x=np.eye(S) - (beta*(lamb**(m-gamma))*Pi)
    x=np.linalg.inv(x)
    y=beta*Pi.dot(lamb**(-gamma))
    nu=x.dot(y)
    Rs=((vhat(t+1)+m)/vhat[t])*(lamb_hat[t+1])
   
    return nu,Rs
    
    
def consolPrice(Pi,lamb,gamma,beta,zeta):
    '''
    Computes price of a consol bond with coupon zeta
    
    Inputs
    --------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * beta - discount factor (float)
    * zeta - coupon (float)
    
    Returns
    ---------
    * pc - price of the consol in each state (length S array)
    * Rc - Return on the consol in each state (length S array)
    '''
    pass
    
    S=len(c)    
    d=np.ones(S)*xi
    pc,Rc = AP.consolPrice(Pi,lamb,gamma,beta,zeta)
    return pc,Rc    
        
    
def callOption(Pi,lamb,gamma,beta,zeta,pStrike,T, epsilon = 1e-8):
    '''
    Computes price of a call option on a consol bond with payoff zeta
    
    Inputs
    --------
    * Pi - Transition matrix (SxS array)
    * lamb - growth rate of consumption (S array)
    * gamma - coefficient of risk aversion (float)
    * zeta - coupon (float)
    * pStrike - strike price (float)
    * T - length of option (can be inf)
    * epsilon - tolerance for infinite horizon problem (float) (bonus question)
    
    Returns
    --------
    * w - price of option in each state of the world
    '''
    pass

    S=len(lamb)
    w={}
    w(T)= np.zeros([T+1,S])
    pc,Rc=consolPrice(Pi,lamb,gamma,beta,zeta)
    for m in range(1,T+1):
        temp=(beta*(lamb**(-gamma))*Pi).dot(w[m-1,:])
        for n in range(S):
            w[m,n]=max(temp[n],pc[n]-pStrike)
    return w[T,:]
     
    
def estimateEquityPremium(Pi_data,lambda_data,gamma,beta,sHist):
    '''
    Estimates the equity premium
    
    Inputs:
    --------
    * Pi_data : estimated transition matrix
    * lambda_data: estimated consumption growth
    * gamma : degree of risk aversion
    * beta : discount rate 
    * sHist : sample path of states
    
    Returns:
    -----------
    * EP : estimate of equity premium
    '''
    pass
    
    def RiskNeutralPrice(Pi,d,beta):
    
        S=len(d)
    temp = np.linalg.inv(np.eye(S)-beta*Pi)
    return temp.dot(beta*Pi.dot(d) )
    
def RiskAversePrice(Pi,d,beta,c,gamma):
    S = len(d)
    dtilde = d * c**(-gamma)
    ptilde = RiskNeutralPrice(Pi,dtilde,beta)
    p = ptilde / c**(-gamma)
    
    R = np.zeros((S,S))
    for s in range(S):
        for sprime in range(S):
            R[s,sprime] = (p[sprime]+d[sprime])/p[s]
    
    for s in range(S):
        R[s,:] = (p+d)/p[s]
        
    return p,R
    
def InfiniteConsolePrice(Pi,xi,beta,c,gamma):
    S = len(c)
    d = np.ones(S)*xi
    return RiskAversePrice(Pi,d,beta,c,gamma)
    
    
def optionTMap(Pi,p,beta,c,gamma,pStrike,what):
    w_execute = p-pStrike
    E_Uc_what = Pi.dot(c**(-gamma)*what) #length S vector
    w_no_execute = beta* E_Uc_what/c**(-gamma)
    
    return np.maximum(w_execute,w_no_execute)
    
def finiteHorizonOption(Pi,xi,beta,c,gamma,pStrike,T):
    S = len(c)
    p = InfiniteConsolePrice(Pi,xi,beta,c,gamma)[0]
    w = {}
    w[0] = np.zeros(S)
    Tf = lambda what : optionTMap(Pi,p,beta,c,gamma,pStrike,what)
    for t in range(0,T):
        w[t+1] = Tf(w[t])
        #equivilent to
        w[t+1] = optionTMap(Pi,p,beta,c,gamma,pStrike,w[t])
    return w[T]

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    